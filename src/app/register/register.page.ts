import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { LocalStorageService } from './../local-storage.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  public Name: string = '';
  public Password: string = '';
  public User = {Name: this.Name, Password: this.Password}
  registerStatus='complete the register'

  constructor(private localStorageService: LocalStorageService) { }

  ngOnInit() {
  }

  onAddUser(){
    this.User.Name = this.Name;
    this.User.Password = this.Password;
    this.localStorageService.setItem(this.Name,  JSON.stringify(this.User))
    return this.registerStatus;
  }

}
